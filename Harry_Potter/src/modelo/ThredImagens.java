package modelo;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import view.Harry;
import view.Jogo;
import view.Vilao;

public class ThredImagens extends Thread {

    Harry harry;
    Jogo jogo;
    private Vilao vilao;
    //thred so para mudar as imagens e fazer os sprites
    public ThredImagens(Harry harry, Jogo jogo, Vilao vilao) {
        this.harry = harry;
        this.jogo = jogo;
        this.vilao = vilao;
    }

    public void run() {
        while (!jogo.isFim()) {
            if (harry.isAndando() && !harry.isAtirando()) {
                harry.mudarImagem();
            } 
                
            else if(!harry.isAtirando() ){
                if (harry.getDirecao() == harry.DIREITA) {
                    harry.getImagem().setIcon(new ImageIcon("src/imagens/harryladod3.png"));
                }
                else if (harry.getDirecao() == harry.ESQUERDA) {
                    harry.getImagem().setIcon(new ImageIcon("src/imagens/harryladoe3.png"));
                }
                else if (harry.getDirecao() == harry.CIMA) {
                    harry.getImagem().setIcon(new ImageIcon("src/imagens/harryt3.png"));
                }
                else if (harry.getDirecao() == harry.BAIXO) {
                    harry.getImagem().setIcon(new ImageIcon("src/imagens/harry2.png"));
                }
            }
            
            // marcador de vida
            // depois eu termino tb 
            
            if(harry.getQteVida() == 7){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida7.png"));                
            }
            else if(harry.getQteVida() == 6){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida6.png"));                
            }
            
            else if(harry.getQteVida() == 5){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida5.png"));                
            }
            else if(harry.getQteVida() == 4){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida4.png"));                
            }
            else if(harry.getQteVida() == 3){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida3.png"));                
            }
            else if(harry.getQteVida() == 2){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida2.png"));                
            }
            else if(harry.getQteVida() == 1){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida1.png"));                
            }
            else if(harry.getQteVida() == 0){
                harry.getVida().setIcon(new ImageIcon("src/imagens/vida0.png"));                
            }
            try {
                sleep(150);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThredImagens.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }
}
