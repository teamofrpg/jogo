package modelo;

import java.util.logging.Level;
import java.util.logging.Logger;
import view.Harry;
import view.Jogo;
import view.Vilao;
//essa therd é para movimenros mais rápidos
//a outra para os mais lentos

public class ThredAndar extends Thread {

    private Jogo jogo;
    private Harry harry;
    private Vilao vilao;

    public ThredAndar(Jogo jogo, Harry harry, Vilao vilao) {
        this.jogo = jogo;
        this.harry = harry;
        this.vilao = vilao;
    }

    public void run() {
        while (!jogo.isFim()) {
            //para ele n andar enqunato atira
            if (harry.isAndando() && !harry.isAtirando()) {
                harry.andar();
            }
            // se tiver tiros no array de tiros ele movimenta
            if (harry.getTiros().size() != 0) {
                for (int i = 0; i < harry.getTiros().size(); i++) {
                    if (harry.getTiros().get(i).getDirecao() == harry.DIREITA) {
                        harry.getTiros().get(i).getImagem().setLocation(harry.getTiros().get(i).getImagem().getX() + 3, harry.getTiros().get(i).getImagem().getY());
                        if(harry.getTiros().get(i).getImagem().getX() > harry.getTiros().get(i).getInicio() + 300){
                        harry.getTiros().get(i).getImagem().setVisible(false);
                        harry.getTiros().remove(i);
                    }
                    } 
                    else if (harry.getTiros().get(i).getDirecao() == harry.ESQUERDA) {
                        harry.getTiros().get(i).getImagem().setLocation(harry.getTiros().get(i).getImagem().getX() - 3, harry.getTiros().get(i).getImagem().getY());
                        if(harry.getTiros().get(i).getImagem().getX() < harry.getTiros().get(i).getInicio() - 300){
                        harry.getTiros().get(i).getImagem().setVisible(false);
                        harry.getTiros().remove(i);
                    }
                    } 
                    else if (harry.getTiros().get(i).getDirecao() == harry.CIMA) {
                        harry.getTiros().get(i).getImagem().setLocation(harry.getTiros().get(i).getImagem().getX(), harry.getTiros().get(i).getImagem().getY() - 3);
                        if(harry.getTiros().get(i).getImagem().getY() < harry.getTiros().get(i).getInicio() - 300){
                        harry.getTiros().get(i).getImagem().setVisible(false);
                        harry.getTiros().remove(i);
                    }
                    } 
                    else if (harry.getTiros().get(i).getDirecao() == harry.BAIXO) {
                        harry.getTiros().get(i).getImagem().setLocation(harry.getTiros().get(i).getImagem().getX(), harry.getTiros().get(i).getImagem().getY() + 3);
                        if(harry.getTiros().get(i).getImagem().getY() > harry.getTiros().get(i).getInicio() + 300){
                        harry.getTiros().get(i).getImagem().setVisible(false);
                        harry.getTiros().remove(i);
                    }
                    }

                    
                }
            }

            try {
                sleep(5);
            } 
            catch (InterruptedException ex) {
                Logger.getLogger(ThredAndar.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
