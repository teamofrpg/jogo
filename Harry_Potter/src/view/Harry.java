package view;

import interfaces.Personagens;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Harry extends Personagem implements Personagens {

    private int cont = 1;
    //aparatar vai ser so uma imagem de fumaça para quando ele sumir e aparecer 
    private JLabel aparatar;
    private boolean aparatar2 = false;
    
    public Harry() {
        vida = new JLabel();
        qteVida = 3;
        vida.setBounds(0, 0, 217, 36);
        tiros = new ArrayList<>();
        escudo = new JLabel();
        imagem = new JLabel();
        imagem.setBounds(50, 50, 31, 33);
        imagem.setIcon(new ImageIcon("src/imagens/harry2.png"));
        imagem.setVisible(true);
        armadura = 0;
        ataque = 100;
        aparatar = new JLabel();
        aparatar.setSize(57, 47);
        aparatar.setIcon(new ImageIcon("src/imagens/aparatar.png"));
        aparatar.setVisible(false);
    }

    @Override
    public void atirar() {
        //aqui ele atira
        Tiro tiro = new Tiro();
        if (direcao == DIREITA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringod.png"));
            imagem.setIcon(new ImageIcon("src/imagens/Direita6_1.png"));
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 79, 25);
            tiro.setInicio(tiro.getImagem().getX());
        } 
        else if (direcao == ESQUERDA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringoe.png"));
            imagem.setIcon(new ImageIcon("src/imagens/Esquerda6_1.png"));
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 79, 25);
            tiro.setInicio(tiro.getImagem().getX());
        } 
        else if (direcao == CIMA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringob.png"));
            imagem.setIcon(new ImageIcon("src/imagens/Tras6_1.png"));
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 25, 79);
            tiro.setInicio(tiro.getImagem().getY());
        }
        else if (direcao == BAIXO) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringoc.png"));
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 25, 79);
            tiro.setInicio(tiro.getImagem().getY());
        }
        // cada tiro no array vai ter cada atributo da classe tiro, ai da para fazer mais coisas com eles
        tiro.setDano(ataque);
        tiro.setDirecao(direcao);
        tiros.add(tiro);
    }

    @Override
    public void andar() {
        //anda 
        if (direcao == DIREITA) {
            imagem.setLocation(imagem.getX() + 1, imagem.getY());
        } else if (direcao == ESQUERDA) {
            imagem.setLocation(imagem.getX() - 1, imagem.getY());
        } else if (direcao == CIMA) {
            imagem.setLocation(imagem.getX(), imagem.getY() - 1);
        } else if (direcao == BAIXO) {
            imagem.setLocation(imagem.getX(), imagem.getY() + 1);
        }
    }

    @Override
    public void mudarImagem() {
        //troca imagem
        if (direcao == DIREITA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/harryladod1.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/harryladod2.png"));
                cont = 1;
            }
        } else if (direcao == ESQUERDA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/harryladoe1.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/harryladoe2.png"));
                cont = 1;
            }
        } else if (direcao == CIMA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/harryt1.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/harryt2.png"));
                cont = 1;
            }
            imagem.setSize(30, 33);
        } else if (direcao == BAIXO) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/harry1.png"));
                cont = 2;
                imagem.setSize(30, 33);
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/harry3.png"));
                cont = 1;
                imagem.setSize(31, 33);
            }

        }
    }

    public JLabel getAparatar() {
        return aparatar;
    }

    public void setAparatar(JLabel aparatar) {
        this.aparatar = aparatar;
    }
    
    public int getCont() {
        return cont;
    }

    public void setCont(int cont) {
        this.cont = cont;
    }

    public boolean isAparatar2() {
        return aparatar2;
    }

    public void setAparatar2(boolean aparatar2) {
        this.aparatar2 = aparatar2;
    }

    

}
