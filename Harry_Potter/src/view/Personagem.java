package view;

import java.util.List;
import javax.swing.JLabel;
//classe pai para vilao e harry
public class Personagem {
    protected JLabel vida;
    protected int qteVida;    
    protected int velocidade = 100;
    protected int direcao;
    public final int DIREITA = 1, ESQUERDA = 2, CIMA = 3, BAIXO = 4;
    protected JLabel imagem;
    protected int ataque;
    protected int armadura;
    protected JLabel escudo;
    protected boolean andando;
    protected List<Tiro> tiros;
    protected boolean atirando;
    
    
    public int getVelocidade() {
        return velocidade;
    }

    public JLabel getVida() {
        return vida;
    }

    public void setVida(JLabel vida) {
        this.vida = vida;
    }

    public int getQteVida() {
        return qteVida;
    }

    public void setQteVida(int qteVida) {
        this.qteVida = qteVida;
    }
    
    public void setVelocidade(int velocidade) {
        this.velocidade = velocidade;
    }

    public int getDirecao() {
        return direcao;
    }

    public void setDirecao(int direcao) {
        this.direcao = direcao;
    }

    public JLabel getImagem() {
        return imagem;
    }

    public void setImagem(JLabel imagem) {
        this.imagem = imagem;
    }

    public int getAtaque() {
        return ataque;
    }

    public void setAtaque(int ataque) {
        this.ataque = ataque;
    }

    public int getArmadura() {
        return armadura;
    }

    public boolean isAtirando() {
        return atirando;
    }

    public void setAtirando(boolean atirando) {
        this.atirando = atirando;
    }
    
    public void setArmadura(int armadura) {
        this.armadura = armadura;
    }

    public boolean isAndando() {
        return andando;
    }

    public void setAndando(boolean andando) {
        this.andando = andando;
    }

    public List<Tiro> getTiros() {
        return tiros;
    }

    public void setTiros(List<Tiro> tiros) {
        this.tiros = tiros;
    }

    public JLabel getEscudo() {
        return escudo;
    }

    public void setEscudo(JLabel escudo) {
        this.escudo = escudo;
    }
    
    
}
