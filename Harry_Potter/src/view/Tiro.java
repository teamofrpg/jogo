package view;

import javax.swing.JLabel;

public class Tiro {
    private int direcao;
    private JLabel imagem;
    private int inicio;
    private int dano;
    
    public Tiro() {
        imagem = new JLabel();
    }

    public int getDano() {
        return dano;
    }

    public void setDano(int dano) {
        this.dano = dano;
    }
    
    public int getDirecao() {
        return direcao;
    }

    public void setDirecao(int direcao) {
        this.direcao = direcao;
    }

    public JLabel getImagem() {
        return imagem;
    }

    public void setImagem(JLabel imagem) {
        this.imagem = imagem;
    }

    public int getInicio() {
        return inicio;
    }

    public void setInicio(int inicio) {
        this.inicio = inicio;
    }
    
}
