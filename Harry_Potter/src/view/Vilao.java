package view;

import interfaces.Personagens;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Vilao extends Personagem implements Personagens{
    int cont = 1;
    //os viloes tb vai ter um array assim como tiros, so q em jogo
    // tem q fazer a ai para eles andarem e atirarem 
    public Vilao() {
        imagem = new JLabel();
        tiros = new ArrayList<>();
    }
    
    @Override
    public void andar() {
        if (direcao == DIREITA) {
            imagem.setLocation(imagem.getX() + 1, imagem.getY());
        } else if (direcao == ESQUERDA) {
            imagem.setLocation(imagem.getX() - 1, imagem.getY());
        } else if (direcao == CIMA) {
            imagem.setLocation(imagem.getX(), imagem.getY() - 1);
        } else if (direcao == BAIXO) {
            imagem.setLocation(imagem.getX(), imagem.getY() + 1);
        }
    }

    @Override
    public void mudarImagem() {
        if (direcao == DIREITA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalDireita.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalDireita2.png"));
                cont = 1;
            }
        } else if (direcao == ESQUERDA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalEsquerda2.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalEsquerda3.png"));
                cont = 1;
            }
        } else if (direcao == CIMA) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalTras.png"));
                cont = 2;
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalTras2.png"));
                cont = 1;
            }
            
        } else if (direcao == BAIXO) {
            if (cont == 1) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalFrente2.png"));
                cont = 2;
                
            } else if (cont == 2) {
                imagem.setIcon(new ImageIcon("src/imagens/ComensalFrente3.png"));
                cont = 1;
                imagem.setSize(31, 33);
            }

        }
    }

    @Override
    public void atirar() {
        Tiro tiro = new Tiro();
        if (direcao == DIREITA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/.png"));
            
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 79, 25);
        } else if (direcao == ESQUERDA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringoe.png"));
            
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 79, 25);
        } else if (direcao == CIMA) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringob.png"));
           
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 25, 79);
        } else if (direcao == BAIXO) {
            tiro.getImagem().setIcon(new ImageIcon("src/imagens/confringoc.png"));
            tiro.getImagem().setBounds(imagem.getX(), imagem.getY(), 25, 79);
        }

        tiro.setDirecao(direcao);
        tiros.add(tiro);
    }
    
}
