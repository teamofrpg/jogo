package view;

import controller.OuvinteJogo;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import modelo.ThredAndar;
import modelo.ThredImagens;

public class Jogo {
    private boolean fim = false;
    private JFrame janela;
    private JLabel fundo;
    //essa imagem n é a final, era so para testar os tiros 
    public Jogo() {
        //sem comentarios
        janela = new JFrame();
        janela.setBounds(0, 0, 800, 600);
        janela.setExtendedState(JFrame.MAXIMIZED_BOTH);
        janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        janela.setVisible(true);
        janela.setLayout(null);
        Vilao vilao = new Vilao();
        janela.add(vilao.getImagem());
        Harry harry = new Harry();
        
        janela.addKeyListener(new OuvinteJogo(this, harry));
        janela.addMouseMotionListener(new OuvinteJogo(this, harry));
        janela.add(harry.getVida());
        janela.add(harry.getImagem());
        janela.add(harry.getAparatar());
        new ThredAndar(this, harry, vilao).start();
        new ThredImagens(harry, this, vilao).start();
        fundo = new JLabel();
        fundo.setIcon(new ImageIcon("src/imagens/planoFundo.jfif"));        
        fundo.setBounds(0, 0, 1400, 750);
        janela.add(fundo);
        janela.addMouseListener(new OuvinteJogo(this, harry));
        fundo.setLocation(-1, -1);
    }

    public boolean isFim() {
        return fim;
    }

    public void setFim(boolean fim) {
        this.fim = fim;
    }

    public JFrame getJanela() {
        return janela;
    }

    public void setJanela(JFrame janela) {
        this.janela = janela;
    }

    public JLabel getFundo() {
        return fundo;
    }

    public void setFundo(JLabel fundo) {
        this.fundo = fundo;
    }
    
}
