package interfaces;

import view.Personagem;

public interface Personagens {
    // todos os metodos dos personagens tem a assinatura aqui
    //obs: para ficar bonito
    public void andar();
    public void mudarImagem();
    public void atirar();
}
