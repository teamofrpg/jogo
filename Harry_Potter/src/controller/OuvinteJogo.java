package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.MouseInputListener;
import view.Harry;
import view.Jogo;
import view.Vilao;

public class OuvinteJogo implements KeyListener, MouseInputListener {
    private Vilao vilao;
    private Jogo jogo;
    private Harry harry;

    public OuvinteJogo(Jogo jogo, Harry harry) {
        this.jogo = jogo;
        this.harry = harry;
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        
        //w, s, a e d são as teclas para andar
        // variavel andando serve para saber se ele ta em movimento ou n 
        
        if (e.getKeyCode() == KeyEvent.VK_W) {
            harry.setDirecao(harry.CIMA);
            harry.setAndando(true);
        } 
        else if (e.getKeyCode() == KeyEvent.VK_S) {
            harry.setDirecao(harry.BAIXO);
            harry.setAndando(true);
        } 
        else if (e.getKeyCode() == KeyEvent.VK_A) {
            harry.setDirecao(harry.ESQUERDA);
            harry.setAndando(true);
        } 
        else if (e.getKeyCode() == KeyEvent.VK_D) {
            harry.setDirecao(harry.DIREITA);
            harry.setAndando(true);
        }
        // o e p serve para atirar e usar o escudo
        // o escudo n é um tiro, para ele n sair andando, e ele so dura enquanto clicar p
        if (e.getKeyCode() == KeyEvent.VK_O) {
            harry.atirar();
            harry.setAtirando(true);
            jogo.getJanela().add(harry.getTiros().get(harry.getTiros().size()-1).getImagem());
            jogo.getFundo().setComponentZOrder(harry.getTiros().get(harry.getTiros().size()-1).getImagem(), 0);
        }
        if(e.getKeyCode() == KeyEvent.VK_P){
            harry.setAtirando(true);
            if(harry.getDirecao() == harry.DIREITA){            
                harry.getImagem().setIcon(new ImageIcon("src/imagens/Direita6_1.png"));
                harry.getEscudo().setIcon(new ImageIcon("src/imagens/protegod.png"));
                harry.getEscudo().setBounds(harry.getImagem().getX()+30, harry.getImagem().getY(), 5, 33);
            }
            else if(harry.getDirecao() == harry.ESQUERDA){                
                harry.getImagem().setIcon(new ImageIcon("src/imagens/Esquerda6_1.png"));
                harry.getEscudo().setIcon(new ImageIcon("src/imagens/protegoe.png"));
                harry.getEscudo().setBounds(harry.getImagem().getX()-6, harry.getImagem().getY(), 5, 33);
            }
            else if(harry.getDirecao() == harry.CIMA){                
                harry.getImagem().setIcon(new ImageIcon("src/imagens/Tras6_1.png"));
                harry.getEscudo().setIcon(new ImageIcon("src/imagens/protegoc.png"));
                harry.getEscudo().setBounds(harry.getImagem().getX() - 2, harry.getImagem().getY() - 6, 33, 5);
            }
            else if(harry.getDirecao() == harry.BAIXO){    
                //fazer a imegem
                harry.getEscudo().setIcon(new ImageIcon("src/imagens/protegob.png"));
                harry.getEscudo().setBounds(harry.getImagem().getX(), harry.getImagem().getY()+36, 33, 5);
            }
            jogo.getJanela().add(harry.getEscudo()); 
            jogo.getFundo().setComponentZOrder(harry.getEscudo(), 0);
            harry.getEscudo().setVisible(true);
        }
        
        
        if(e.getKeyCode() == KeyEvent.VK_E){
            harry.setAparatar2(true);
        }
    }
    
    @Override
    public void keyReleased(KeyEvent e) {
        //sem comentarios
        harry.setAndando(false);
        harry.getEscudo().setVisible(false);
        harry.setAtirando(false);
        harry.setAparatar2(false);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        //tem q fazer a imagem de 'aparatar' desaparecer depois de um tempo 
        //tem q ter um tempo de recarga 
        if(harry.isAparatar2()){
            harry.getImagem().setIcon(new ImageIcon("src/imagens/harrymorto.png"));
            jogo.getJanela().add(harry.getAparatar());      
            harry.getAparatar().setLocation(harry.getImagem().getX(), harry.getImagem().getY());
            harry.getImagem().setLocation(e.getLocationOnScreen());
            jogo.getFundo().setComponentZOrder(harry.getAparatar(), 0);
            harry.getImagem().setLocation(e.getLocationOnScreen());                      
            harry.getImagem().setVisible(true);
            harry.getAparatar().setVisible(true);
        }
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

}
